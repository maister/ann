# ANN (Add New Note)

`ann` is a simple shell script that makes it really easy to add and view your
notes. Think of `ann` as your personal assistant. You can record whatever text
you want into a file in many ways, from typing a random thought out, pasting
something from your clipboard, or saving the output of a program.

This script simply makes plain text files to store your thoughts, which is
great because:

- Plain text is lightweight, occupying very little of your storage
- Plain text is easy to back up and share to other devices
- Plain text is compatible with `find` to locate a day's journal
- Plain text can be `grep` to find a specific note

### Your notes are timestamped and organized by auto-dated files

Upon running the script for the first time, you will have the option to create
auto-dated `YYYY-MM-DD-notes.txt` files or a central `notes.txt` file. Further,
each entry is auto-timestamped, so no matter what you choose, you will always
know when each note was made.

Let's say I have chosen to have dated notes files. It's January 27th, 2021, and
I just learned something cool. So, I tell `ann`, `ann I learned something
today!`. `ann` would create a file in my `ANN_DIRECTORY/2021-01-27-notes.txt`
file and append `[2021-01-27 10:00:00 AM] I learned something today!` to the
file.

Then, later on in the same day, I run `ann something else I want to note`. `ann` will
continue to append to that file `[2021-01-27 12:00:00 PM] something else I want to note`.

And so on and so forth until I have a running list of every thought I had. Cool, huh?

## You're ready to try it out?

Clone the repository and move the script into your `PATH` directory. For example.

```sh
git clone https://gitlab.com/maister/ann
cp ann/ann $HOME/.local/bin
export PATH=$PATH:$HOME/.local/bin
```

Then, you can run the script as `ann`.

A safer but less convenient alternative is to run the script locally. You can
move the script to a directory of your choice.

```sh
git clone https://gitlab.com/maister/ann
cp ann/ann /path/to/your/directory/
```

With this method, you need to run the script as `/path/to/your/directory/ann`.
You may consider aliasing `n` to this by `alias n="/path/to/your/directory/ann"`.

### Configuration

Running the script for the first time will prompt for you to create it at the
default directory, which is located at `$HOME/notes`. If you want your notes
stored elsewhere, you can change this directory by:

1. Put `export ANN_DIRECTORY="/foo/bar"` in your `~/.bashrc`, `~/.zshrc`, or equivalent
file and create the `/foo/bar` notes directory with
```sh
mkdir -p /foo/bar
```
2. Directly edit this script and replace `$HOME/notes` with `/foo/bar` in the
`ANN_DIRECTORY` variable

Additionally, if not already done so, you'll want to define your `EDITOR` in
your `~/.bashrc` or `~/.zshrc`. For example, in my `.zshrc` is
`export EDITOR="nvim"`. By default, the script will use `nano` if not defined.
Vimmers and Emacs-ers will want to replace it with your editor of choice.

## Usage Examples

There's 3 ways of using this script:

- `ann something you want to jot down`
  - Appends whatever arguments you add as text into the notes file

- `xclip -o | ann`
  - Pipes and appends anything (in this case your clipboard's contents) into
the notes file

- `ann`
  - Opens the notes file in your configured `EDITOR`

That's really all there is to it. You have the power of the command line at
your finger tips to manipulate these files however you see fit. For example you
can run `cat 2019-12-*.txt > 2019-12.txt` to create a monthly summary file. Or,
since this is just a Bash script, you can freely modify its contents to your
liking.

## License

This script is licensed under [GNU General Public License](LICENSE.md).
